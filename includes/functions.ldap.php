<?php
/**
 *
 * @package     Plugin
 * @subpackage  LDAP Authentication
 * @id          $Id$:
 * @author      Stephan Lang <stephan.lang@outlook.com>
 * @copyright   Stephan Lang
 * @link        http://www.evidente.de
 * 
 */

defined('CON_FRAMEWORK') || die('Illegal call: Missing framework initialization - request aborted.');

global $cfg;

/**
 * Sets language of client, like done in front_content.php
 *
 * @param  int  $client  Client id
 */
function ldap_setClientLanguageId($client) {
    global $lang, $load_lang, $cfg;

    if ((int) $lang > 0) {
        // there is nothing to do
        return;
    } elseif ($load_lang) {
        // use the first language of this client, load_client is set in __FRONTEND_PATH__/data/config/config.php
        $lang = $load_lang;
        return;
    }

    // try to get clients language from table
    $sql = "SELECT B.idlang FROM "
            . $cfg['tab']['clients_lang'] . " AS A, "
            . $cfg['tab']['lang'] . " AS B "
            . "WHERE "
            . "A.idclient='" . ((int) $client) . "' AND A.idlang=B.idlang"
            . "LIMIT 0,1";

    if ($aData = ldap_queryAndNextRecord($sql)) {
        $lang = $aData['idlang'];
    }
}

/**
 * Loads Advanced Mod Rewrite configuration for passed client using serialized
 * file containing the settings.
 *
 * File is placed in /cms/data/config/CON_ENVIRONMENT and is named like config.auth_ldap.php.
 *
 * @param  int   $clientId     Id of client
 * @param  bool  $forceReload  Flag to force to reload configuration, e. g. after
 *                             done changes on it
 */
function ldap_loadConfiguration($clientId, $forceReload = false) {
    global $cfg;
    static $aLoaded;

    $clientId = (int) $clientId;
    if (!isset($aLoaded)) {
        $aLoaded = array();
    } elseif (isset($aLoaded[$clientId]) && $forceReload == false) {
        return;
    }

    $mrConfig = ldap_getConfiguration($clientId);

    if (is_array($mrConfig)) {
        // merge auth ldap configuration with global cfg array
        $cfg = array_merge($cfg, $mrConfig);
    } else {
        // couldn't load configuration, set defaults
        $backendPath = cRegistry::getBackendPath();
        include_once($backendPath . $cfg['path']['plugins'] . 'auth_ldap/includes/config.auth_ldap_default.php');
    }

    $aLoaded[$clientId] = true;
}

/**
 * Returns the mod rewrite configuration array of an client.
 *
 * File is placed in /cms/data/config/CON_ENVIRONMENT and is named like config.auth_ldap.php.
 *
 * @param   int   $clientId     Id of client
 * @return  array|NULL
 */
function ldap_getConfiguration($clientId) {
    global $cfg;

    $clientId = (int) $clientId;

    $backendPath = cRegistry::getBackendPath();

    $clientConfig = cRegistry::getClientConfig($clientId);
    $fePath = $clientConfig['path']['frontend'];

    $file = $fePath . 'data/config/' . CON_ENVIRONMENT . '/config.auth_ldap.php';

    if (!is_file($file) || !is_readable($file)) {
        $file = $backendPath . $cfg['path']['plugins'] . 'auth_ldap/includes/config.auth_ldap_' . $clientId . '.php';
    }

    if (!is_file($file) || !is_readable($file)) {
        return NULL;
    }
    if ($content = cFileHandler::read($file)) {
        return unserialize($content);
    } else {
        return NULL;
    }
}

/**
 * Saves the mod rewrite configuration array of an client.
 *
 * File is placed in /cms/data/config/CON_ENVIRONMENT and is named like config.auth_ldap.php.
 *
 * @param   int    $clientId     Id of client
 * @param   array  $config       Configuration to save
 * @return  bool
 */
function ldap_setConfiguration($clientId, array $config) {
    global $cfg;

    $clientId = (int) $clientId;

    $clientConfig = cRegistry::getClientConfig($clientId);
    $fePath = $clientConfig['path']['frontend'];

    $file = $fePath . 'data/config/' . CON_ENVIRONMENT . '/config.auth_ldap.php';
    $result = cFileHandler::write($file, serialize($config));

    $file = $backendPath . $cfg['path']['plugins'] . 'auth_ldap/includes/config.auth_ldap_' . $clientId . '.php';
    if (is_file($file) && is_writeable($file)) {
        cFileHandler::remove($file, serialize($config));
    }

    return ($result) ? true : false;
}


function ldap_queryAndNextRecord($query) {
    static $db;
    if (!isset($db)) {
        $db = cRegistry::getDb();
    }
    if (!$db->query($query)) {
        return NULL;
    }
    return ($db->nextRecord()) ? $db->getRecord() : NULL;
}


/**
 * Add \ to the Hex Value of the objectGUID, thats the Format required for LDAP search using the object GUID
 *
 * @param   string    $hexGUID     objectGUID in HEX format as String
 *
 * @return  string    HEX Value as Formated String Example: \\67\\53\\16\\0f\\70\\22\\ee\\43\\b1\\e5\\6f\\8d\\98\\f0\\06\\e8
 */
function ldap_FormatHexGUID($hexGUID)
{
    $output="";
    for ($i = 0; $i <= strlen($hexGUID)-2; $i = $i+2){
        $output .= "\\".substr($hexGUID, $i, 2);
    }
    return $output;
}


/**
 * Convert the binary value ob objectGUID from LDAP to Hex
 *
 * @param   string    $binaryGUID     objectGUID in binary format as String
 *
 * @return  string    HEX Value Example: 6753160f7022ee43b1e56f8d98f006e8
 */
function ldap_binaryGUIDtoHex($binaryGUID)
{
    return strtoupper( bin2hex($binaryGUID) );
}


/**
 * Convert the binary value to String
 *
 * @param   string    $binaryGUID     objectGUID in binary format as String
 *
 * @return  string    String Value Example: 0f165367-2270-43ee-b1e5-6f8d98f006e8
 */
function ldap_binaryGUIDtoString($binaryGUID) 
{
    $hexGUID = ldap_binaryGUIDtoHex($binaryGUID);
    return ldap_hexGUIDtoString($hexGUID);
}


/**
 * Convert the HEX objectGUID to String 
 *
 * @param   string    $hexGUID     objectGUID in HEX format as String
 *
 * @return  string    String Value Example: 0f165367-2270-43ee-b1e5-6f8d98f006e8
 */
function ldap_hexGUIDtoString($hexGUID)
{
    $guidinhex = str_split($hexGUID, 2);
    $guid = "";
    //Take the first 4 octets and reverse their order
    $first = array_reverse(array_slice($guidinhex, 0, 4));
    foreach($first as $value)
    {
        $guid .= $value;
    }
    $guid .= "-";
    // Take the next two octets and reverse their order
    $second = array_reverse(array_slice($guidinhex, 4, 2, true), true);
    foreach($second as $value)
    {
        $guid .= $value;
    }
    $guid .= "-";
    // Repeat for the next two
    $third = array_reverse(array_slice($guidinhex, 6, 2, true), true);
    foreach($third as $value)
    {
        $guid .= $value;
    }
    $guid .= "-";
    // Take the next two but do not reverse
    $fourth = array_slice($guidinhex, 8, 2, true);
    foreach($fourth as $value)
    {
        $guid .= $value;
    }
    $guid .= "-";
    //Take the last part
    $last = array_slice($guidinhex, 10, 16, true);
    foreach($last as $value)
    {
        $guid .= $value;
    }
    return $guid;
}

function ldap_getGroupByHexGUID($hexGUID)
{
    $cfg = cRegistry::getConfig();
    $formatedHexGUID = ldap_FormatHexGUID($hexGUID);

    $serverFQDN = $cfg['auth_ldap']['serverFQDN'];
    $netbiosName = $cfg['auth_ldap']['netbiosName'];
    $frontendGroupSearchPath = $cfg['auth_ldap']['frontendGroupSearchPath'];
    $adServer = "ldap://".$serverFQDN;
    $ldap = ldap_connect($adServer);
    ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
    ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);

    // by default you can not anonymously search a ldap directory
    $username = $cfg['auth_ldap']['ldapServiceUser'];
    $password = $cfg['auth_ldap']['ldapServiceUserPassword'];

    $ldaprdn = $netbiosName."\\". $username;
    $bind = @ldap_bind($ldap, $ldaprdn, $password);
    
    if ($bind) {
        $filter = "(objectguid=".$formatedHexGUID.")";
        $result = ldap_search($ldap, $frontendGroupSearchPath, $filter, array("distinguishedname"));
        $info = ldap_get_entries($ldap, $result);
        @ldap_close($ldap);
        return $info[0]['distinguishedname'][0];
    }else{
        @ldap_close($ldap);
        return false;
    }
}

function ldap_getAllGroups()
{
    $cfg = cRegistry::getConfig();

    $serverFQDN = $cfg['auth_ldap']['serverFQDN'];
    $netbiosName = $cfg['auth_ldap']['netbiosName'];
    $frontendGroupSearchPath = $cfg['auth_ldap']['frontendGroupSearchPath'];
    $adServer = "ldap://".$serverFQDN;
    $ldap = ldap_connect($adServer);
    ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
    ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);

    // by default you can not anonymously search a ldap directory
    $username = $cfg['auth_ldap']['ldapServiceUser'];
    $password = $cfg['auth_ldap']['ldapServiceUserPassword'];

    $ldaprdn = $netbiosName."\\". $username;
    $bind = @ldap_bind($ldap, $ldaprdn, $password);
    
    // default for new mappings
    $groups['0'] = "Bitte LDAP Gruppe auswaehlen";

    if ($bind) {
        $filter = "(objectClass=group)";
        $result = ldap_search($ldap, $frontendGroupSearchPath, $filter, array("objectguid", "distinguishedname"));
        $info = ldap_get_entries($ldap, $result);
        for ($i=0; $i<$info["count"]; $i++)
        {
            $hexGUID = ldap_binaryGUIDtoHex($info[$i]['objectguid']['0']); // .":". $info['0']['distinguishedname']['0'];
            $groups[$hexGUID] = $info[$i]['distinguishedname']['0'];
        }
    }else {
        return false;
    }

    return $groups;
}

/**
 * Check is a Contenido Frontend user exists
 *
 * @param   string    $username     Contenido Frontend user username
 *
 * @return Item|bool   next object, or false if no objects
 */
function getFrontendUserIfExists($username){
    $client = cRegistry::getClientId();
    $frontendUserColl = new cApiFrontendUserCollection();
    $frontendUserColl->select("idclient = ".$client." AND username = '".$username."'");
    return $frontendUserColl->next();
}

/**
 * Create a Contenido Frontend user
 *
 * @param   array    $info     all Data from LDAP Search
 *
 * @return Item|bool   created user object, or false
 */
function createFrontendUserFromLDAP($username, $info){

    $cfg = cRegistry::getConfig();

    $client = cRegistry::getClientId();
    $userAsHexGUID  = ldap_binaryGUIDtoHex($info[0]["objectguid"][0]);
    $firstname      = $info[0]["givenname"][0];
    $lastname       = $info[0]["sn"][0];
    $title          = $info[0]["title"][0];
    $department     = $info[0]["department"][0];
    $company        = $info[0]["company"][0];
    $phone          = $info[0]["telephonenumber"][0];
    $mobile         = $info[0]["mobile"][0];
    $mail           = $info[0]["mail"][0];
    $customFields = array();

    // LDAP Custom Field mappings
    for ($x=1; $x<10; $x++){
        $thisField = $cfg['auth_ldap']['customField'.$x];
        if ($thisField  != null ){
            $customFields[$x] = $info[0][ strtolower($thisField) ][0]; 
        }
    }

    // create user
    $feUsers = new cApiFrontendUserCollection();
    $feUser = $feUsers->create($username);
    $feUser->set('valid_from', date('Y-m-d H:i:s'), false);
    $feUser->set('valid_to','0000-00-00 00:00:00', false);
    $feUser->set('active', 1);
    $feUser->store();
    $idfrontenduser = $feUser->get('idfrontenduser');

    $feLDAPUsers = new LDAPFrontendUserCollection();
    $feLDAPUser = $feLDAPUsers->create($idfrontenduser, $firstname, $lastname, $title, $department, $company, $phone, $mobile, $mail, $customFields, $userAsHexGUID, $client);

    return $feUser;
}

function syncAllUsers(){

}

/**
 * Delete the Additional Data in LDAP Users when a Contenido Frontend user is deleted
 *
 * @param   int    $idfrontenduser    the id of the deleted user
 *
 */
function ldapFrontendUserDeleted($idfrontenduser){
    $client = cRegistry::getClientId();
    $LDAPFrontendUsers = new LDAPFrontendUserCollection();
    if ($LDAPFrontendUsers->select("idclient = ".$client." AND idfrontenduser = '".$idfrontenduser."'")) {
        $LDAPFrontendUser = $LDAPFrontendUsers->next();
        $idldapuser = $LDAPFrontendUser->get('idldapuser');
        $LDAPFrontendUsers->delete($idldapuser);
    }
}

// these functions also exist in another include of another plugin
if (!function_exists('filterHandler')) {
    // filterHandler ( $selection, "jobCategory" )
    function filterHandler($selection, $field)
    {
        // add filters here
        if (is_array($selection)) {
            $c=0;
            $sWhereTemp = " AND ".$field." IN (";
            foreach ($selection as $item) {
                if ($c!=0) {
                    $sWhereTemp .= ", ";
                }
                $sWhereTemp .= "'".$item."'";
                $c++;
            }
            return $sWhereTemp.")";
        } else {
            return "";
        }
    }
}

if (!function_exists('getRequestData')) {
    function getRequestData($requestData, $field)
    {
        $selection = (isset($requestData[$field])) ? $requestData[$field] : '';
        return $selection;
    }
}

?>