<?php
/**
 * Plugin LDAP Authentication default settings. This file will be included if
 * auth_ldap settings of an client couldn't loaded.
 *
 * NOTE:
 * Changes in these settings will affect all clients, as long
 * as they don't have their own configuration.
 * PHP needs write permissions to the folder, where this file resides.
 *
 * @package     Plugin
 * @subpackage  LDAP Authentication
 * @id          $Id$:
 * @author      Stephan Lang <stephan.lang@outlook.com>
 * @copyright   Stephan Lang
 * @link        http://www.evidente.de
 * 
 */

defined('CON_FRAMEWORK') || die('Illegal call: Missing framework initialization - request aborted.');

$cfg = cRegistry::getConfig(); 
global $cfg;

$client = cRegistry::getClientId();
$clientObject = new cApiClient($client);

//all these settings can be used as $cfg['auth_ldap']['SETTINGSNAME']

// User for reading ldap
$ldapcfg['ldapServiceUser']             = 'ldap_read';
$ldapcfgDesc['ldapServiceUser']         = 'User for reading from LDAP';

// Password for ldapServiceUser
$ldapcfg['ldapServiceUserPassword']     = 'ldap_read';
$ldapcfgDesc['ldapServiceUserPassword'] = 'Password for ldapServiceUser';

// the FQDN of the LDAP Server
$ldapcfg['serverFQDN']                  = 'domaincontroller.domain.local';
$ldapcfgDesc['serverFQDN']              = 'FQDN or IP of the LDAP Server';

// the logon netbios domain
$ldapcfg['netbiosName']                 = 'DOMAIN';
$ldapcfgDesc['netbiosName']             = 'The netbios domain name';

// LDAP attribute for username
$ldapcfg['frontendUserLDAPUsername']      = 'sAMAccountName';
$ldapcfgDesc['frontendUserLDAPUsername']  = 'LDAP attribute for username, currently only supports sAMAccountName for AD DS';

// DN From where users are searched
$ldapcfg['frontendUserSearchPath']      = 'CN=Users,DC=domain,DC=local';
$ldapcfgDesc['frontendUserSearchPath']  = 'DN From where users are searched for frontend user sync';

// DN From where groups are searched
$ldapcfg['frontendGroupSearchPath']     = 'OU=Groups,DC=domain,DC=local';
$ldapcfgDesc['frontendGroupSearchPath'] = 'DN From where groups are searched for frontend group mapping';

// Fields to get from LDAP
$fieldsFromLDAP = array("sn", "givenname", "title", "department", "company", "telephonenumber", "mobile", "mail", "objectguid", "distinguishedname"); 

// LDAP Custom Field mappings
for ($x=1; $x<10; $x++){
    $ldapcfg['customField'.$x]     = null;
    $ldapcfgDesc['customField'.$x] = 'Name of an LDAP Attribute to map to the custom Field'.$x;
}

foreach ($ldapcfg as $key => $value) 
{
    $setting = $clientObject->getProperty('auth_ldap', $key, $client);
    if ($setting == false){
        $setting = $clientObject->setProperty('auth_ldap', $key, $value);
    }
    $cfg['auth_ldap'][$key] = getEffectiveSetting('auth_ldap', $key, $value);
    $testtt = $cfg['auth_ldap'][$key];
}

foreach ($ldapcfgDesc as $key => $value) 
{
    $cfg['auth_ldap_desc'][$key] = $value;
}

// LDAP Custom Field mappings
for ($x=1; $x<10; $x++){
    array_push($fieldsFromLDAP, strtolower($cfg['auth_ldap']['customField'.$x]) );
}

$cfg['auth_ldap']['fieldsFromLDAP'] = $fieldsFromLDAP;

// static config: templates
// frontend group mapping
$cfg['auth_ldap']['template_frontendgroupmapping'] = $cfg['path']['plugins'] . 'auth_ldap/templates/template.frontendgroupmapping.tpl';

// settings
$cfg['auth_ldap']['template_settings'] = $cfg['path']['plugins'] . 'auth_ldap/templates/template.settings.tpl';


// setting that should not be editable in backend
$cfg['auth_ldap']['hide_settings'] = array('hide_settings', 'fieldsFromLDAP', 'template_frontendgroupmapping', 'template_settings');