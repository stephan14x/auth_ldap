<?php

/**
 *
 * @package Plugin
 * @subpackage Frontend LDAP Authentication
 * @author Stephan Lang <stephan.lang@outlook.com>
 * @copyright Stephan Lang
 * @link http://www.evidente.de
 */

// assert CONTENIDO framework
defined('CON_FRAMEWORK') || die('Illegal call: Missing framework initialization - request aborted.');
global $cfg;
$sessionId = cRegistry::getSession()->id;


// new Job Collection
$users = new LDAPFrontendUserCollection();
$sWhereClient = "idclient = ".$client;
$sWhere = null;

// filter stuff
$filterFields = array("firstname", "lastname", "department", "phone", "mobile");
$filterFieldsDescription = array("firstname" => "Vorname", 
                                 "lastname" => "Nachname", 
                                 "department" => "Abteilung", 
                                 "phone" => "Telefon", 
                                 "mobile" => "Handy"
                                );
$selectedOptions = array();
$allOptions = array();

foreach($filterFields as $field){
    // initialize all arrays
    $selectedOptions[$field] = array();
    // get the request values from filter selections
    $selectedOptions[$field] = getRequestData($_REQUEST, $field);
    // and build where part of select
    $sWhere .= filterHandler($selectedOptions[$field], $field);
    // initialize all arrays for all Filter values
    $allOptions[$field] = array();
}

// build where for select with clientid and filters
$sWhere = "idclient = ".$client . $sWhere;
$users->select($sWhere);
$usersArray = $users->fetchArray('idldapuser', $users->fileds);

// arrays with all filter options
foreach($usersArray as $user)
{
    foreach ($filterFields as $field) {
        array_push($allOptions[$field], $user[$field]);
        $allOptions[$field] = array_unique($allOptions[$field]);
    }
}

// use smarty template to output header text
$smarty = cSmartyBackend::getInstance();
$smarty->assign('users', $usersArray);
$smarty->assign('path', $cfg['path']['plugins']);
$smarty->assign('filterFields', $filterFields);
$smarty->assign('filterFieldsDescription', $filterFieldsDescription);
$smarty->assign('tableHeaders', $users->fileds);
$smarty->assign("allOptions", $allOptions);
$smarty->assign("selectedOptions", $selectedOptions);

$smarty->display( $cfg['path']['plugins'] . 'auth_ldap/templates/template.users.tpl');





?>