<?php

/**
 * Plugin LDAP Authentication initialization file.
 *
 * This file will be included by CONTENIDO plugin loader routine, and the content
 * of this file ensures that the LDAP Authentication will be initialized correctly.
 *
 * @package Plugin
 * @subpackage LDAP Authentication
 * @author Stephan Lang <stephan.lang@outlook.com>
 * @copyright Stephan Lang
 * @link http://www.evidente.de
 */

// assert CONTENIDO framework
defined('CON_FRAMEWORK') || die('Illegal call: Missing framework initialization - request aborted.');

global $_cecRegistry, $cfg, $contenido, $area, $client, $load_client;

// initialize client id
if (isset($client) && (int) $client > 0) {
    $clientId = (int) $client;
} elseif (isset($load_client) && (int) $load_client > 0) {
    $clientId = (int) $load_client;
} else {
    $clientId = '';
}

plugin_include("auth_ldap", "includes/config.ldap_default.php");
plugin_include("auth_ldap", "includes/functions.ldap.php");
plugin_include("auth_ldap", "classes/class.ldap.frontendgroupmapping.php");

// no way to override core classes here
/*
$pluginClassPath = 'contenido/plugins/auth_ldap/classes/';
cAutoload::addClassmapConfig(array(
    'AuthLDAPBase'                      => $pluginClassPath . 'class.authldapbase.php',
    'AuthLDAP'                          => $pluginClassPath . 'class.authldap.php',
    'LDAPGroupMapping'                  => $pluginClassPath . 'class.authldap.groupmapping.php',
    'LDAPGroupMappingCollection'        => $pluginClassPath . 'class.authldap.groupmapping.php',
    'cAuthHandlerFrontend' => $pluginClassPath.'class.auth.handler.frontend.ldap.php'
));
unset($pluginClassPath);

unset($clientId);*/


$_cecRegistry = cApiCecRegistry::getInstance();

// Add new function to CONTENIDO Extension Chainer
$_cecRegistry->addChainFunction('Contenido.Permissions.FrontendUser.AfterDeletion', 'ldapFrontendUserDeleted');

?>