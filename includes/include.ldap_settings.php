<?php

/**
 *
 * @package Plugin
 * @subpackage Frontend LDAP Authentication
 * @author Stephan Lang <stephan.lang@outlook.com>
 * @copyright Stephan Lang
 * @link http://www.evidente.de
 */

// assert CONTENIDO framework
defined('CON_FRAMEWORK') || die('Illegal call: Missing framework initialization - request aborted.');
global $cfg;
$sessionId = cRegistry::getSession()->id;

$action = (isset($_REQUEST['action'])) ? $_REQUEST['action'] : 'index';
$edit = (isset($_REQUEST['setting'])) ? $_REQUEST['setting'] : '0';
$settingvalue = (isset($_REQUEST['settingvalue'])) ? $_REQUEST['settingvalue'] : '0';


if ($action == "ldap_save_settings") 
{
    $clientObject = new cApiClient($client);
    $settings = $clientObject->getProperties();
    $setting = $clientObject->setProperty('auth_ldap', $edit, $settingvalue);
    $cfg['auth_ldap'][$edit] = $settingvalue;
}

$keys = array();

foreach( $cfg['auth_ldap'] as $key => $value )
{
    if ( !in_array($key, $cfg['auth_ldap']['hide_settings'] ) ) {
         array_push($keys, $key);
    }
}

// use smarty template to output header text
$smarty = cSmartyBackend::getInstance();
$smarty->assign('keys', $keys);
$smarty->assign('auth_ldap', $cfg['auth_ldap']);
$smarty->assign('area', 'auth_ldap');
$smarty->assign("idclient", $client);
$smarty->assign('auth_ldap_desc', $cfg['auth_ldap_desc']);
$smarty->assign('edit', $edit); 
$smarty->assign('sid', $sessionId);
$smarty->assign('action', $action);
$smarty->display($cfg['auth_ldap']['template_settings']);





?>