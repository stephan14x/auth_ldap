<?php

/**
 *
 * @package Plugin
 * @subpackage Frontend LDAP Authentication
 * @author Stephan Lang <stephan.lang@outlook.com>
 * @copyright Stephan Lang
 * @link http://www.evidente.de
 */

// assert CONTENIDO framework
defined('CON_FRAMEWORK') || die('Illegal call: Missing framework initialization - request aborted.');
global $cfg;
$sessionId = cRegistry::getSession()->id;

$action = (isset($_REQUEST['action'])) ? $_REQUEST['action'] : 'index';
$edit = (isset($_REQUEST['idfrontendgroupmapping'])) ? $_REQUEST['idfrontendgroupmapping'] : '0';

if ($action == "ldap_delete_frontendgroupmapping") 
{
    $feGroupMappings = new LDAPFrontendGroupMappingCollection();
    $feGroupMappings->delete($_REQUEST['idfrontendgroupmapping']);
    unset($feGroupMappings);
}
if ($action == "ldap_save_frontendgroupmapping") 
{
    $feGroupMapping = new LDAPFrontendGroupMapping($_REQUEST['idfrontendgroupmapping']);
    $feGroupMapping->set("idfrontendgroup", $_REQUEST['idfrontendgroup']);
    $feGroupMapping->set("objectguidhex", $_REQUEST['objectguidhex']);
    $feGroupMapping->set("idclient", $idclient);
    $feGroupMapping->store();
    unset($feGroupMapping);
}

if ($action == "ldap_new_frontendgroupmapping") 
{
    $feGroupMappings = new LDAPFrontendGroupMappingCollection();
    $feGroupMapping = $feGroupMappings->create($_REQUEST['idfrontendgroup'], '0', $client);
    $edit = $feGroupMapping->get('idfrontendgroupmapping');
    $action = "ldap_edit_frontendgroupmapping";

    unset($feGroupMapping);
    unset($feGroupMappings);
}



//get all frontend groups
$feGroups = new cApiFrontendGroupCollection();
$feGroups->select("idclient = ".$client);
while (($feGroup = $feGroups->next()) !== false) {
    $groups[$feGroup->get("idfrontendgroup")] = $feGroup->get("groupname");
}

//get all group mappings
$feGroupMappings = new LDAPFrontendGroupMappingCollection();
$feGroupMappings->link('cApiFrontendGroupCollection');
$feGroupMappings->addResultField('objectguidhex');
$feGroupMappings->addResultField('cApiFrontendGroupCollection.groupname');
$feGroupMappings->setWhere('LDAPFrontendGroupMappingCollection.idclient', $client);
$feGroupMappings->setOrder('cApiFrontendGroupCollection.groupname');
$feGroupMappings->query();

$fields['idfrontendgroupmapping'] = 'idfrontendgroupmapping';
$fields['idfrontendgroup'] = 'idfrontendgroup';
$fields['objectguidhex'] = 'objectguidhex';
$fields['groupname'] = 'groupname';
$table = $feGroupMappings->fetchTable($fields);

$mappings = array();

foreach ($table as $key => $item) 
{
    $mapping = array(
        'idfrontendgroupmapping'    => $item['idfrontendgroupmapping'],
        'idfrontendgroup'           => $item['idfrontendgroup'],
        'objectguidhex'             => $item['objectguidhex'],
        'groupname'                 => $item['groupname']
        );
    array_push($mappings, $mapping);
    unset($groups[$item['idfrontendgroup']]);
}



// use smarty template to output header text
$smarty = cSmartyBackend::getInstance();
$smarty->assign('id', $objectguidhex);
$smarty->assign('names', $dn);
$smarty->assign('area', 'auth_ldap_frontendgroupmapping');
$smarty->assign("idclient", $client);
$smarty->assign('mappings', $mappings);
$smarty->assign('edit', $edit); 
$smarty->assign('sid', $sessionId);
$smarty->assign('action', $action);

$smarty->assign('groups', $groups);
$smarty->assign('ldap_groups', ldap_getAllGroups() );

$smarty->display($cfg['auth_ldap']['template_frontendgroupmapping']);



?>