<?php
$pluginClassPath = 'contenido/plugins/auth_ldap/classes/';
return array(
    'cAuthHandlerFrontend'                  => $pluginClassPath . 'class.auth.handler.frontend.ldap.php',
    'LDAPFrontendGroupMapping'              => $pluginClassPath . 'class.ldap.frontendgroupmapping.php',
    'LDAPFrontendGroupMappingCollection'    => $pluginClassPath . 'class.ldap.frontendgroupmapping.php',
    'LDAPFrontendUser'                      => $pluginClassPath . 'class.ldap.user.php',
    'LDAPFrontendUserCollection'            => $pluginClassPath . 'class.ldap.user.php',
//    'cAuthHandlerBackend' => 'contenido/plugins/auth_ldap/classes/class.auth.handler.backend.ldap.php'
);
?>