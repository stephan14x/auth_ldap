# Contenido LDAP Authentication Plugin

Uses LDAP for Contenido Frontend Users, maps LDAP Groups to Contenido Groups

## Getting Started

### Prerequisites

Contenido 4.9 or newer
Tested only on latest development code

You need a LDAP server, and a service user with permissions to read from LDAP
Testet omly with a vanilla Windows Server 2019 Active Directory

### Installing

Download all file and put them in your contenido/plugins directory

Go to Conteido Backend -> Administration -> Plugin Manager and install the plugin

You find the Plugin in Extras -> LDAP

Change the Settings in Conteido Backend -> Extras -> LDAP -> Settings to point to your LDAP directory

Create a frontend group and assign permissions as you normally would.

Go to Conteido Backend -> Extras -> LDAP -> Frontend Group Mapping and assign LDAP groups to the contenido groups

Login to frontend using an LDAP user

## License

This project is licensed under "GNU General Public License v3.0" - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Thanks to all contributing to contenido
* Special thanks to http://forum.contenido.org users xmurrix and Oldperl


## Related

http://www.contenido.org

http://git.contenido.org:7990/scm/con/contenido.git