<html>
<head>
    <link rel="stylesheet" type="text/css" href="styles/contenido.css">
</head>
<body class="page_generic page_frontendgroupmapping">
    <h2>Frontend Group Mapping</h2>
    <br>
    <h3>How To</h3>
    <p>Create a frontend group and assign permissions as you normally would. </p>
    <p>Then return to this page and map the Contenido frontend group to a group from LDAP directory.</p>
    <p>Next time a user logs in with his LDAP credentials, he is automatically added to the frontend group, if he is a member in the mapped LDAP group.</p>
    <p>Group membership is updated on every login.</p>
    <p>Nested group memberships work as well.</p>
    <table class="generic" cellspacing="0" cellpadding="2">
        <tr>
            <th>
                Contenido Groupname
            </th>
            <th>
                LDAP Group DN
            </th>
            <th>
                Actions
            </th>
        </tr>

        <!-- BEGIN:BLOCK -->
        {foreach item=mapping from=$mappings}
        <tr>
                <td>
                    {$mapping.groupname|escape}
                </td>
                <td>
                    {if $edit == $mapping.idfrontendgroupmapping && $action == "ldap_edit_frontendgroupmapping"}
                    <form method="post" action="main.php?area={$area}&frame=4&action=ldap_save_frontendgroupmapping&idclient={$idclient}&contenido={$sid}">
                        <input type="hidden" name="idfrontendgroupmapping" value="{$mapping.idfrontendgroupmapping}">
                        <input type="hidden" name="idfrontendgroup" value="{$mapping.idfrontendgroup}">
                        <select name="objectguidhex">
                            {html_options options=$ldap_groups selected=$mapping.objectguidhex}
                        </select>
                        <input type="image" src="images/but_ok.gif" title="Save mapping">
                        <a id="m10" href="main.php?area={$area}&frame=4&idclient={$idclient}&contenido={$sid}"><img src="images/but_cancel.gif"
                                alt="Abbrechen" title="Abbrechen"></a>
                    </form>
                    {else}
                        {$ldap_groups[$mapping.objectguidhex]}
                    {/if}
                </td>
                <td>
                    {if $action != "ldap_edit_frontendgroupmapping"}
                    <a id="m12" href="main.php?area={$area}&frame=4&action=ldap_edit_frontendgroupmapping&idclient={$idclient}&idfrontendgroupmapping={$mapping.idfrontendgroupmapping}&contenido={$sid}"><img
                            src="images/editieren.gif" alt="Bearbeiten" title="Bearbeiten"></a>&nbsp;&nbsp;&nbsp;
                    <a id="m11" href="main.php?area={$area}&frame=4&action=ldap_delete_frontendgroupmapping&idclient={$idclient}&idfrontendgroupmapping={$mapping.idfrontendgroupmapping}&contenido={$sid}"><img
                            src="images/but_delete.gif" alt="Löschen" title="Löschen"></a>
                    {/if}
                </td>
        </tr>
        {/foreach}
        <!-- END:BLOCK -->

        {assign var=groups_count value=$groups|@count} 
            <tr>
                <td colspan="2">
                    {if $groups_count > 0}
                        <form method="post" action="main.php?area={$area}&frame=4&action=ldap_new_frontendgroupmapping&{$idclient}&contenido={$sid}">
                            New Mapping for <select name="idfrontendgroup">
                                {html_options options=$groups }
                            </select>
                            <input type="image" src="images/folder_new.gif" title="New mapping">
                        </form>
                    {else}No more unmapped frontend groups available.
                    {/if}
                </td>
                <td>
                </td>
            </tr>
    </table>
</body>

</html>