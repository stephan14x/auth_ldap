<html>

<head>
    <link rel="stylesheet" type="text/css" href="styles/contenido.css">
</head>

<body class="page_generic page_users">
    <h2>Users</h2>
    <br><br>

    <form action="" id="filter" name="filter" method="post">

        {foreach item=filterField from=$filterFields}
        <select data-placeholder="Begin typing to filter {$filterField} ..." multiple="multiple" class="js-example-basic-multiple" id="{$filterField}" name="{$filterField}[]">
            {html_options values=$allOptions.$filterField output=$allOptions.$filterField selected=$selectedOptions.$filterField}
        </select> 
        {/foreach}
        <input type="submit" value="Filter anwenden" id="submitForm">
        <input type="button" value="Filter zurücksetzen" id="clearForm">
    </form>
    <br><br>
    <table class="generic" cellspacing="0" cellpadding="2">
        <tr>
            {foreach item=description from=$tableHeaders}
            <th>
                {$description}
            </th>
            {{/foreach}}
        </tr>

        <!-- BEGIN:BLOCK -->
        {foreach item=user from=$users}
        <tr>
            {foreach item=element from=$user}
            <td>
                {$element}
            </td>
            {{/foreach}}
        </tr>
        {/foreach}
        <!-- END:BLOCK -->


    </table>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>


    <script language="javascript">
        var runningTimeout;
        {foreach item=filterField from=$filterFields}
            $('#{$filterField}').select2( { width: '250px', placeholder: 'Filtern nach {$filterFieldsDescription.$filterField}', allowClear: true, closeOnSelect:false } );
            $('#{$filterField}').on("select2:close", function (e) { formSubmit(); } );
            $('#{$filterField}').on("select2:select", function (e) { formSubmitDelayed(); } );
            $('#{$filterField}').on("select2:unselect", function (e) { formSubmit(); } );
        {/foreach}
 
            function formSubmit()
            {
                $('form#filter').submit(); 
            }

            function formSubmitDelayed()
            {
                clearTimeout(runningTimeout);
                runningTimeout = setTimeout(function () { formSubmit(); }, 5000);
            }

            function clearAllFilter() {
                {foreach item=filterField from=$filterFields}
                    $('#{$filterField}').val(null).trigger('change');
                {/foreach}
                formSubmit()
            }

            $( "#clearForm" ).click(function() { clearAllFilter(); } );

    </script>

</body>

</html>