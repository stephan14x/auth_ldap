<html>
<head>
    <link rel="stylesheet" type="text/css" href="styles/contenido.css">
</head>
<body class="page_generic page_frontendgroupmapping">
    <h2>Plugin LDAP Authentication Settings</h2>
    <br>
    <h3>Manuall setup steps</h3>
    <p>For the Plugin to work you need to move the file 'contenido/plugins/auth_ldap/autoloader/config.autoloader.local.php' to 'data/config/&lbrace;environment&rbrace;'' or preferable create a symlink there.</p>
    <p>After plugin installation, all values are set to defaults and you must set them to real world values of your LDAP system.</p>
    <p>A user in our LDAP directory is required to read the groups for the group map configuration page. <br>Please use a seperate service account as the password is currently stored in plain text and visible in contenido backend.<br>Suggestions to improve this are welcome.</p>
    <table class="generic" cellspacing="0" cellpadding="2" >
        <tr>
            <th>
                Setting
            </th>
            <th>
                Description
            </th>
            <th>
                Value
            </th>
            <th>
                Action
            </th>
        </tr>

        <!-- BEGIN:BLOCK -->
        {foreach item=key from=$keys}
        <tr>
            <td>
                {$key}
            </td>
            <td>
                {$auth_ldap_desc.$key}
            </td>
            <td>
                {if $edit == $key && $action == "ldap_edit_settings"}
                    <form method="post" action="main.php?area={$area}&frame=4&action=ldap_save_settings&idclient={$idclient}&contenido={$sid}">
                        <input type="hidden" name="setting" value="{$key}">
                        <input type="text" name="settingvalue" value="{$auth_ldap.$key}" />
                        <input type="image" src="images/but_ok.gif" title="Save mapping">
                        <a id="m10" href="main.php?area={$area}&frame=4&idclient={$idclient}&contenido={$sid}"><img
                            src="images/but_cancel.gif" alt="Abbrechen" title="Abbrechen"></a>
                    </form>
                {else}
                    {$auth_ldap.$key}
                {/if}
            </td>
            <td>
                {if $action != "ldap_edit_settings"}
                <a id="m12" href="main.php?area={$area}&frame=4&action=ldap_edit_settings&idclient={$idclient}&setting={$key}&contenido={$sid}"><img
                        src="images/editieren.gif" alt="Bearbeiten" title="Bearbeiten"></a>
                {/if}
            </td>
        </tr>
        {/foreach}
        <!-- END:BLOCK --> 

    </table>

</body>

</html>