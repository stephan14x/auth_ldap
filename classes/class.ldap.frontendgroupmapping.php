<?php
 
class LDAPFrontendGroupMappingCollection extends ItemCollection {
    public function __construct() {
        global $cfg;
        parent::__construct($cfg['sql']['sqlprefix'] . "_pi_ldap_frontendgroupmapping", 'idfrontendgroupmapping');
        $this->_setItemClass('LDAPFrontendGroupMapping');
        $this->_setJoinPartner('cApiFrontendGroupCollection');
    }

    public function create($idfrontendgroup, $objectguidhex, $idclient) {
        $item = parent::createNewItem();
        $item->set("idfrontendgroup", $idfrontendgroup);
        $item->set("objectguidhex", $objectguidhex);
        $item->set("idclient", $idclient);
        $item->store();
        return $item;

    }
}

class LDAPFrontendGroupMapping extends Item {
    public function __construct($mId = false) {
        global $cfg;
        parent::__construct($cfg['sql']['sqlprefix'] . "_pi_ldap_frontendgroupmapping", 'idfrontendgroupmapping');
        $this->setFilters(array(
            'addslashes'
        ), array(
            'stripslashes'
        ));
        if ($mId !== false) {
            $this->loadByPrimaryKey($mId);
        }
    }
}