<?php

/**
 * This file contains the Frontend LDAP Authentication handler class.
 *
 * @package    Plugin
 * @subpackage Frontend LDAP Authentication
 * @author     Stephan Lang <stephan.lang@outlook.com>
 * @copyright  Stephan Lang
 * @link       http://www.evidente.de
 * 
 */

defined('CON_FRAMEWORK') || die('Illegal call: Missing framework initialization - request aborted.');

/**
 * This class is the Frontend LDAP Authentication handler for CONTENIDO.
 *
 * @package    Plugin
 * @subpackage Frontend LDAP Authentication
 */
class cAuthHandlerFrontend extends cAuthHandlerAbstract {

    /**
     *
     * @var bool
     */
    protected $_defaultNobody = true;

    /**
     * Constructor to create an instance of this class.
     *
     * Automatically sets the lifetime of the authentication to the
     * configured value.
     */
    public function __construct() {
        $cfg = cRegistry::getConfig();
        $this->_lifetime = (int) $cfg['frontend']['timeout'];
        if ($this->_lifetime == 0) {
            $this->_lifetime = 15;
        }
    }

    /**
     * Handle the pre authorization.
     * Returns a valid user ID to be set before the login form is handled,
     * otherwise false.
     *
     * @see cAuthHandlerAbstract::preAuthorize()
     * @return string|false
     */
    public function preAuthorize() {
        $password = $_POST['password'];

        if ($password == '') {
            // Stay as nobody when an empty password is passed
            $this->auth['uname'] = $this->auth['uid'] = self::AUTH_UID_NOBODY;

            return false;
        }

        return $this->validateCredentials();
    }

    /**
     * Display the login form.
     * Includes a file which displays the login form.
     *
     * @see cAuthHandlerAbstract::displayLoginForm()
     */
    public function displayLoginForm() {
        include(cRegistry::getFrontendPath() . 'front_crcloginform.inc.php');
    }

        
    /** Validate the credentials against LDAP.
     *
     * Validate the users input against LDAP and return a valid user
     * ID or false.
     *
     * @see cAuthHandlerAbstract::validateCredentials()
     * @return string|false
     */
     
    public function validateCredentials() 
    {
        $cfg = cRegistry::getConfig();
        $client = cRegistry::getClientId();
        $serverFQDN = $cfg['auth_ldap']['serverFQDN'];
        if ($serverFQDN == null){ return false; }
        $netbiosName = $cfg['auth_ldap']['netbiosName'];
        $frontendUserSearchPath = $cfg['auth_ldap']['frontendUserSearchPath'];
        $frontendGroupSearchPath = $cfg['auth_ldap']['frontendGroupSearchPath'];
        
        $adServer = "ldap://".$serverFQDN;
        $ldap = ldap_connect($adServer);
        $username = cString::toLowerCase(conHtmlentities(stripslashes(trim($_POST['username']))));
        $password = $_POST['password'];
        $ldaprdn = $netbiosName."\\". $username;
        $memberOfAsHexGUID = array();

        ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);

        $bind = @ldap_bind($ldap, $ldaprdn, $password);
    
        if ($bind) {
            $filter="(&(!(useraccountcontrol:1.2.840.113556.1.4.803:=2))(".$cfg['auth_ldap']['frontendUserLDAPUsername']."=".$username."))";
            $result = ldap_search($ldap, $frontendUserSearchPath, $filter, $cfg['auth_ldap']['fieldsFromLDAP'] ); 

            $info = ldap_get_entries($ldap, $result);
            // this LDAP search should only return one entry, otherwise something is wrong and we will fail authentication
            if($info['count'] > 1) {
                return false;    
            }
               
            $userDn = $info[0]["distinguishedname"][0];

            //region check admin/nested group
            $filter = "(member:1.2.840.113556.1.4.1941:=".$userDn.")"; 
            $result = ldap_search($ldap, $frontendGroupSearchPath, $filter, array("name", "objectguid", "distinguishedname"));
            $memberOfInfo = ldap_get_entries($ldap, $result);
            
            for ($x=0; $x<$memberOfInfo["count"]; $x++) {
                array_push($memberOfAsHexGUID, ldap_binaryGUIDtoHex($memberOfInfo[$x]["objectguid"][0]));
            }
            
            @ldap_close($ldap);
        
            // create user on logon
            // find user if exists
            if($feUser = getFrontendUserIfExists($username) ){
                // remove user from all groups
                $idfrontenduser = $feUser->get("idfrontenduser");
                $feGroups = $feUser->getGroupsForUser();
                if(count($feGroups)>0){
                    $groupmembers = new cApiFrontendGroupMemberCollection();
                    foreach ($feGroups as $idfrontendgroup) {
                        $groupmembers->remove($idfrontendgroup, $idfrontenduser);
                    }
                }
            } else {
                // create 
                $feUser = createFrontendUserFromLDAP($username, $info);
                $idfrontenduser = $feUser->get('idfrontenduser');
            }

            // get all group mappings
            $feGroupMappings = new LDAPFrontendGroupMappingCollection();
            $feGroupMappings->select("idclient = ".$client);
            // match groups by GUID as HEX
            while (($feGroupMapping = $feGroupMappings->next()) !== false) {
                $feGroupMappingHexGUID = $feGroupMapping->get("objectguidhex");
                $idfegroup = $feGroupMapping->get("idfrontendgroup");

                $key = array_search($feGroupMappingHexGUID, $memberOfAsHexGUID, false);
                if($key !== false)
                {
                    $groupmembers = new cApiFrontendGroupMemberCollection();
                    $groupmembers->create($idfegroup, $idfrontenduser);
                }
            }
      
            // ldap user only for frontend access
            $this->auth['perm'] = "frontend";
            return $idfrontenduser;
        }else {
            return false;    
        }
    }

    /**
     * Log the successful authentication.
     *
     * Frontend logins won't be logged.
     *
     * @see cAuthHandlerAbstract::logSuccessfulAuth()
     */
    public function logSuccessfulAuth() {
        return;
    }

    /**
     * Returns true if a user is logged in.
     *
     * @see cAuthHandlerAbstract::isLoggedIn()
     * @return bool
     */
    public function isLoggedIn() {
        $authInfo = $this->getAuthInfo();

        if(isset($authInfo['uid'])) {
            $user = new cApiUser($authInfo['uid']);
            $frontendUser = new cApiFrontendUser($authInfo['uid']);

            return $user->get('user_id') != '' || $frontendUser->get('idfrontenduser') != '';
        } else {
            return false;
        }
    }
    
    

    
    
}