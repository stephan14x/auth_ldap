<?php
 
class LDAPFrontendUserCollection extends ItemCollection {
    public function __construct() {
        global $cfg;
        parent::__construct($cfg['sql']['sqlprefix'] . "_pi_ldap_user", 'idldapuser');
        $this->_setItemClass('LDAPFrontendUser');
        $this->_setJoinPartner('cApiFrontendUserCollection');
    }
    
    public $fileds = array( 'idfrontenduser', 
                            'firstname', 
                            'lastname', 
                            'title', 
                            'department',
                            'company',
                            'phone',
                            'mobile',
                            'mail',
                            'customField1',
                            'customField2',
                            'customField3',
                            'customField4',
                            'customField5',
                            'customField6',
                            'customField7',
                            'customField8',
                            'customField9',
                            'objectguidhex',
                            'idclient'
                        );

    public function create($idfrontenduser, $firstname, $lastname, $title, $department, $company, $phone, $mobile, $mail, $customFields, $objectguidhex, $idclient) {
        $item = parent::createNewItem();
        $item->set("idfrontenduser", $idfrontenduser);
        $item->set("firstname", $firstname);
        $item->set("lastname", $lastname);
        $item->set("title", $title);
        $item->set("department", $department);
        $item->set("company", $company);
        $item->set("phone", $phone);
        $item->set("mobile", $mobile);
        $item->set("mail", $mail);
        for ($x=1; $x<10; $x++) {
            $item->set("customField".$x, $customFields[$x]);
        }
        $item->set("objectguidhex", $objectguidhex);
        $item->set("idclient", $idclient);
        $item->store();
        return $item;

    }
}

class LDAPFrontendUser extends Item {
    public function __construct($mId = false) {
        global $cfg;
        parent::__construct($cfg['sql']['sqlprefix'] . "_pi_ldap_user", 'idldapuser');
        $this->setFilters(array(
            'addslashes'
        ), array(
            'stripslashes'
        ));
        if ($mId !== false) {
            $this->loadByPrimaryKey($mId);
        }
    }
}